==================== Assignment 1 Information ====================
README - Datastructures

INTRODUCTION
- This assignment i did by myself, here's an excerpt from its README file (which can
be found in the assignment folder)

RUNNING THE PROGRAM
- You require a terminal in order to run it (For example via putty)
- I have provided the executable, so compiling isn't needed
- Simply in the run ./test_datastructures (Assuming the file is in the same directory) and use the below inputs

- The example commands i used were (assuming in same directory):
* NOTE: 1. You can specify any directory you like, it won't complain i think
        2. Order doesn't matter for the flag/parameter pairs, it still works
        (You do still have to group them though, so -s followed by the datastructure)
        3. Files will be saved with a .csv extension

 ./test_datastructures -s set -d data/dict.dat -t data/kaddath.txt -o _setOut
 ./test_datastructures -s vector -d data/dict.dat -t data/kaddath.txt -o _vectorOut
 ./test_datastructures -s list -d data/dict.dat -t data/kaddath.txt -o _listOut
 ./test_datastructures -s custom_list -d data/dict.dat -t data/kaddath.txt -o _customListOut
 ./test_datastructures -s custom_tree -d data/dict.dat -t data/kaddath.txt -o _customTreeOut

==================== Assignment 2 Information ====================
README - Draughts Game

INTRODUCTION
- This is a project i did along with a friend for a C++ course 
(I did the main crux of coding, friend worked on class diagram, report and assisted with coding)
- Startup skeleton code provided by tutor - Paul Miller
- Assignment focused on the MVC convention - Model, View, Controller

RUNNING THE PROGRAM
- You require a terminal in order to run it (For example via putty)
- I have provided the executable, so compiling isn't needed
- Simply in the run ./draughts (Assuming the file is in the same directory)
- Follow the instructions to play the game, the game catches error cases when you type something wrong, it is input sensitive

########################################
* From submission README *

AUTHORS
Pacific Thai - s3429648
Rei Ito - s3607050

CONTRIBUTORS
Paul Miller (Start up)

FILES
Makefile
src/(*.cpp, *.h)
obj/(*.o, *.d)

INSTALL
- make
- ./draughts

BUGS
- None known at this time

########################################