
/* COSC1254 - Programming using C++ - Assignment 1
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 */

#ifndef INPUTCHECK_H
#define INPUTCHECK_H

#include "util.h"

/* NOTE: Function usage
 * validSuffix() == check extension of dictionary and text files
 * validateArgs() == ensure correct flags have been used, and correct arguments */

bool validSuffix(std::string, std::string);
std::vector<std::string> validateArgs(char * args[], std::vector<std::string>,
                                                     std::vector<std::string>);

#endif
