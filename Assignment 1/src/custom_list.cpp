
/* COSC1254 - Programming using C++ - Assignment 1
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 */

#include "custom_list.h"

using cl=custom_list;

// ########## Implementations of node functions inside custom_list class
// ***** GETTERS AND SETTERS *****
// Setting next node for pointer to node passed in
void cl::node::setNext(std::unique_ptr<cl::node> &&newnext){
  nextNode = std::move(newnext);
}

// Get next node in list
cl::node * cl::node::getNext(void) {
  return nextNode.get();
}

// Get string data from node
std::string cl::node::getData(void) {
  return data;
}

// ***** CUSTOM_LIST FUNCTIONS *****
cl::node * cl::getHead(void) {
  return head.get();
}

void cl::add(std::string data) {
  // Create new node based on data passed in
  std::unique_ptr<cl::node> newnode = std::make_unique<cl::node>(data);
  // Initialize string to use for comparisons
  std::string nodeData;

  // [CASE 1] - list is empty (head == nullptr), add new node to list
  if(head == nullptr)
    head = std::make_unique<node>(data);

  // [CASE 2] - list not empty, add it as the new head
  else {
    newnode->setNext(std::move(head));
    head = std::move(newnode);
  }
}

void cl::compareList(std::map<std::string, int> &dataMap,
                     std::map<std::string, std::string> &extraMap,
                     custom_list &dictList) {
  /* NOTE: Logic in this works the same way as of the other datastructures found
   * in container.cpp in function searchData(), so only light commenting added
   */
  node * textEle = head.get(); // Node belonging to first list containing text data
  node * dictEle = dictList.getHead(); // Node belonging to second list with dictionary data
  std::string line = "";       // Lines found in data_custom_list's nodes
  std::string key = "";        // All dictionary key terms
  // Same variables as container's searchData()
  int minEditDistance, editDistance;
  bool wordFound = false;
  std::string word = "";

  while(textEle != nullptr) {
    /* Create local variable first then tokenize, passing textEle->getData() directly
     * leads to errors due to processing from the getData() function */
    line = textEle->getData();
    tokenizer tokens(line, DELIMETER);
    for(tokenizer::iterator i = tokens.begin(); i != tokens.end(); ++i) {
      minEditDistance = getEditDistance(*i, dictEle->getData());
      while (dictEle != nullptr) {
        key = dictEle->getData(); // Store dictionary element text into variable

        // Edit distance related calculations
        editDistance = getEditDistance(*i, key);
        if (editDistance < minEditDistance) {
          minEditDistance = editDistance;
          word = key;
        } else if (editDistance == minEditDistance) {
          word.append(" ");
          word.append(key);
        }

        // [A.] - Word Found
        if (!key.compare(*i)) {
          if(dataMap.find(*i) != dataMap.end())
            dataMap[*i]++;
          else
            dataMap.insert(std::pair<std::string, int>(*i, 1));
        }
        if (wordFound) {
          extraMap.insert(std::pair<std::string, std::string>(*i, ""));
          break;
        }
        dictEle = dictEle->getNext(); // Iterate to the next dictionary term
      }
      // [B.] - Word not found
      if (!wordFound) {
        extraMap.insert(std::pair<std::string, std::string>(*i, word));
        dataMap.insert(std::pair<std::string, int>(*i, 0));
      }

      dictEle = dictList.getHead(); // Once end reached reset back to head
      wordFound = false;
    }
    textEle = textEle->getNext(); // Go to text line of text
  }
}
