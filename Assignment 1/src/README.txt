/* COSC1254 - Programming using C++ - Assignment 1
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 */

- Everything should be done, but not to full perfect completion though in terms
of quality, the comments in the code will hopefully explain further enough

- With the edit-distance implementation/fuzzy word checking, the results aren't
uniform so they may look different depending on the datastructure you choose

- I couldn't get the binary search tree to store more than one fuzzy word, so
its just going to show one word output for the similar words.

- Other than that all datastructures should work, theres also progress printing
to identify at what stage the program is at.

- The example commands i used were (assuming in same directory):
* NOTE: 1. You can specify any directory you like, it won't complain i think
        2. Order doesn't matter for the flag/parameter pairs, it still works
        (You do still have to group them though, so -s followed by the datastructure)
        3. Files will be saved with a .csv extension

 ./test_datastructures -s set -d data/dict.dat -t data/kaddath.txt -o _setOut
 ./test_datastructures -s vector -d data/dict.dat -t data/kaddath.txt -o _vectorOut
 ./test_datastructures -s list -d data/dict.dat -t data/kaddath.txt -o _listOut
 ./test_datastructures -s custom_list -d data/dict.dat -t data/kaddath.txt -o _customListOut
 ./test_datastructures -s custom_tree -d data/dict.dat -t data/kaddath.txt -o _customTreeOut
