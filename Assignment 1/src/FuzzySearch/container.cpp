
/* COSC1254 - Programming using C++ - Assignment 1
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 */

#include "container.h"

/* NOTE: Alot of the functions are going to look like a series of if/elses,
 * I'm not exactly sure how to make it more efficient as switches do not work with
 * the std::string type. */

/* NOTE: There will be repetitive code, as i'm not exactly sure how to create
 * a container type which can switch between each datastructure */

/* NOTE: Idea for map value update obtained from:
 * http://www.cplusplus.com/forum/general/85171/ */

void container::addTextData(std::string data) {
  if(datastructure == "custom_list")
    data_custom_list.add(data);
  else if(datastructure == "custom_tree")
    data_custom_tree.add(data);
  else if(datastructure == "list")
    data_list.push_front(data);
  else if(datastructure == "vector")
    data_vector.push_back(data);
  else
    data_set.insert(data); // set is naturally sorted, so sorting isn't required
  /* For the last check: else if(!datastructure.compare("set")) {
   * can be used, but error checking should work so final check isnt needed */
}

void container::addDictData(std::string data){
  /* Essentially same function as addTextData(), this could technically has been
   * one function, but then it would've added another layer of if's to each, so
   * i decided to separate it into two, one called for text and other for dictionary
   */
  if(datastructure == "custom_list")
    dict_custom_list.add(data);
  else if(datastructure == "custom_tree")
    dict_custom_tree.add(data);
  else if(datastructure == "list")
    dict_list.push_front(data);
  else if(datastructure == "vector")
    dict_vector.push_back(data);
  else
    dict_set.insert(data);
}

void container::searchData(std::map<std::string, int> &dataMap,
                           std::map<std::string, std::string> &extraMap) {
  /* NOTE: All datastructures use the same method to search and compare,
   * the only difference being the variable being passed in the loops.
   * For custom_list and custom_tree, there are slight modifications but the logic
   * remains somewhat the same */

  int minEditDistance, editDistance;  // Distance calculation tracking
  bool wordFound = false;             // Enables non-full iterations through dictionary
  std::string word = "";              // Fuzzy word to store

  if (datastructure == "list") {
    for (auto dataEle : data_list) {
      tokenizer tokens(dataEle, DELIMETER); // Tokenize each element in the list
      // Create an iterator for the tokenizer and go through each token
      for(tokenizer::iterator i = tokens.begin(); i != tokens.end(); ++i) {
        /* Search through dictionary for the specific word
         * For first word, store it as default minimum */
        minEditDistance = getEditDistance(*i, dict_list.front());
        for (auto dictEle : dict_list) {
          // Obtain edit distance
          editDistance = getEditDistance(*i, dictEle);
          // Afterwards for every other comparison, compare to see if its the new min
          if (editDistance < minEditDistance) {
            minEditDistance = editDistance;
            word = dictEle;
          } else if (editDistance == minEditDistance) { // Append for subsequent matches
            word.append(" ");
            word.append(dictEle);
          }

          // [A.] Word is found in the dictionary
          if (dictEle == *i) {
            wordFound = true;
            if(dataMap.find(*i) != dataMap.end()) // Check if element already in map
              dataMap[*i]++;                       // If so update its count
            else // Else insert new, with a count of 1
              dataMap.insert(std::pair<std::string, int>(*i, 1));
          }
          // Immediately exit when word is found, no need to continue
          if (wordFound) {
            /* Blank value enables both maps to be iterated over at same time in main*/
            extraMap.insert(std::pair<std::string, std::string>(*i, ""));
            break;
          }
        }
        // [B.] Word could not be found in the dictionary
        if (!wordFound) {
          extraMap.insert(std::pair<std::string, std::string>(*i, word));
          dataMap.insert(std::pair<std::string, int>(*i, 0));
        }
        wordFound = false; // Reset for the next word to not being found
      }
    }
  }

  if (datastructure == "set") {
    for (auto dataEle : data_set) {
      tokenizer tokens(dataEle, DELIMETER);
      for(tokenizer::iterator i = tokens.begin(); i != tokens.end(); ++i) {
        minEditDistance = getEditDistance(*i, *dict_set.begin());
        for (auto dictEle : dict_set) {
          editDistance = getEditDistance(*i, dictEle);
          if (editDistance < minEditDistance) {
            minEditDistance = editDistance;
            word = dictEle;
          } else if (editDistance == minEditDistance) {
            word.append(" ");
            word.append(dictEle);
          }
          if (dictEle == *i) {
            wordFound = true;
            if(dataMap.find(*i) != dataMap.end())
              dataMap[*i]++;
            else
              dataMap.insert(std::pair<std::string, int>(*i, 1));
          }
          if (wordFound) {
            extraMap.insert(std::pair<std::string, std::string>(*i, ""));
            break;
          }
        }
        if (!wordFound) {
          extraMap.insert(std::pair<std::string, std::string>(*i, word));
          dataMap.insert(std::pair<std::string, int>(*i, 0));
        }
        wordFound = false;
      }
    }
  }

  if (datastructure == "vector") {
    for (auto dataEle : data_vector) {
      tokenizer tokens(dataEle, DELIMETER);
      for(tokenizer::iterator i = tokens.begin(); i != tokens.end(); ++i) {
        minEditDistance = getEditDistance(*i, dict_vector.front());
        for (auto dictEle : dict_vector) {
          editDistance = getEditDistance(*i, dictEle);
          if (editDistance < minEditDistance) {
            minEditDistance = editDistance;
            word = dictEle;
          } else if (editDistance == minEditDistance) {
            word.append(" ");
            word.append(dictEle);
          }
          if (dictEle == *i) {
            wordFound = true;
            if(dataMap.find(*i) != dataMap.end())
              dataMap[*i]++;
            else
              dataMap.insert(std::pair<std::string, int>(*i, 1));
          }
          if (wordFound) {
            extraMap.insert(std::pair<std::string, std::string>(*i, ""));
            break;
          }
        }
        if (!wordFound) {
          extraMap.insert(std::pair<std::string, std::string>(*i, word));
          dataMap.insert(std::pair<std::string, int>(*i, 0));
        }
        wordFound = false;
      }
    }
  }

  if (datastructure == "custom_list")
    data_custom_list.compareList(dataMap, extraMap, dict_custom_list);

  if (datastructure == "custom_tree")
    data_custom_tree.compareTree(dataMap, extraMap, dict_custom_tree);
}
