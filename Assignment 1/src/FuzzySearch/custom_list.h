
/* COSC1254 - Programming using C++ - Assignment 1
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 * Code base + ideas gotten from file: intlist.h provided in course example files
 */

#ifndef CUSTOM_LIST_H
#define CUSTOM_LIST_H

#include <memory>
#include <iostream>
#include "util.h"

class custom_list {
  // Custom list's inner class (node)
  class node {
    std::string data;                 // String data being stored at the node
    std::unique_ptr<node> nextNode;   // Pointer to next node connection

    public:
      // Constructor: set data to newdata, and null to nextNode
      node(std::string newdata) : data(newdata), nextNode(nullptr) {}
      void setNext(std::unique_ptr<node> &&);
      node *getNext(void);
      std::string getData(void);
      friend class custom_list; // Enables node to use variables from custom_list
  };

  std::unique_ptr<node> head; // Head node of custom_list

  public:
    // Default constructor for custom list, set head to null
    custom_list(void) : head(nullptr) {}
    node *getHead(void);
    void add(std::string);
    void compareList(std::map<std::string, int> &,
                     std::map<std::string, std::string> &, custom_list &);
};

#endif
