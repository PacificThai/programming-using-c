
/* COSC1254 - Programming using C++ - Assignment 1
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 */

#include "custom_tree.h"

using ct=custom_tree;

// ***** Implementations of node functions inside custom_tree class *****
std::string ct::node::getData(void){
  return data;
}

void ct::node::add(std::string newData) {
  /* This function is recursive and will keep calling itself in order to Find
   * the correct position to add in the new node, shifting left/rightwards */

  // Move left, when current node is greater than new data to add
  if (data.compare(newData) > 0){
    // IF: Current node already has a left child, call function again using leftNode
    if (left)
      return left->add(newData);
    // Else: Current node's left child is empty (nullptr), create new left child
    else
      left = std::make_unique<ct::node>(newData);
  }

  // Move right, when current node is lesser than new data to add
  else if (data.compare(newData) < 0){
    // IF: Current node already has a right child, call function again using rightNode
    if (right)
      return right->add(newData);
    // Else: Current node's left child is empty (nullptr), create new right child
    else
      right = std::make_unique<ct::node>(newData);
  }

  // Update count (when same data is added)
  else
    dataCount++;
}

void ct::node::search(std::string text, std::map<std::string, int> &dataMap,
                      std::map<std::string, std::string> &extraMap, int minDistance) {
  // bool wordUpdate = false;
  int minEditDistance = minDistance;
  bool updateMap = false;
  std::string closest = "";

  // NOTE: data = dictionary term, text = tokenized word from line in text tree
  // When there is a left child, call same function for left child with updated params
  if(left)
     left->search(text, dataMap, extraMap, minEditDistance);

  // Perform editDistance search for fuzzy words
  editDistance = getEditDistance(text, data);
  if (editDistance < minEditDistance) {
    minEditDistance = editDistance;
    closest = data;
    updateMap = true;
  }

  // Search for the term in dictionary, same logic as search in other datastructures
  if (data == text) {
    if(dataMap.find(text) != dataMap.end())
      dataMap[text]++;
    else {
      extraMap.insert(std::pair<std::string, std::string>(text, ""));
      dataMap.insert(std::pair<std::string, int>(text, 1));
    }
  }
  else {
    if (updateMap) {
      // If key already exists, remove it to update, dataMap not touched
      if(extraMap.find(text) != extraMap.end())
        extraMap.erase(text);

      extraMap.insert(std::pair<std::string, std::string>(text, closest));
      dataMap.insert(std::pair<std::string, int>(text, 0));
    }
  }
  // When there is a right child, also repeat for right, pass in updated params
  if(right)
     right->search(text, dataMap, extraMap, minEditDistance);
}

void ct::node::compare(std::map<std::string, int> &dataMap,
          std::map<std::string, std::string> &extraMap, custom_tree &dictTree) {
  std::string startKey = dictTree.getRoot()->getData();
  int startingDistance;

  // Function same structure as search function
  if(left)
     left->compare(dataMap, extraMap, dictTree);

  // If line has been loaded in more than once, repeat dataCount times
  for (int d = dataCount; d > 0; d--) {
    // Tokenize node in dataTree then pass into recursive search() in dictTree
    tokenizer tokens(data, DELIMETER);
    for(tokenizer::iterator i = tokens.begin(); i != tokens.end(); i++) {
      startingDistance = getEditDistance(*i, startKey);
      dictTree.search(*i, dataMap, extraMap, startingDistance);
    }
  }

  if(right)
     right->compare(dataMap, extraMap, dictTree);
}

// ***** CUSTOM_TREE FUNCTIONS *****
ct::node * ct::getRoot(void){
  return root.get();
}

void ct::add(std::string newData){
  // If root already exists, call node's add function to add child
  if(root)
    return root->add(newData);
  // Otherwise set root to new data
  root = std::make_unique<ct::node>(newData);
}

void ct::search(std::string term, std::map<std::string, int> &dataMap,
                std::map<std::string, std::string> &extraMap, int minDistance) {
  root->search(term, dataMap, extraMap, minDistance);
}

void ct::compareTree(std::map<std::string, int> &dataMap,
                     std::map<std::string, std::string> &extraMap,
                     custom_tree &dictTree) {
  root->compare(dataMap, extraMap, dictTree);
}
