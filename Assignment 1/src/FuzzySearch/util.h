
/* COSC1254 - Programming using C++ - Assignment 1
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 */

#ifndef UTIL_H
#define UTIL_H

// Datastructures that are required by specs
#include <vector>
#include <list>
#include <set>                 // Used for both set/multiset
#include <map>                 // For storing data from dictionary search
// Other included libraries
#include <algorithm>           // Enables find(), compare()
#include <stdlib.h>            // For exit, EXIT_FAILURE
#include <iostream>            // For std namespace
#include <fstream>             // Enable ifstream/ofstream (Input/Output filestream)
#include <boost/tokenizer.hpp> // Tokenizing lines for file reading

/* Boost related variables: tokenizer definition and character separator
 * NOTE: Since specs don't have newline character "\n" as a requirement,
 * blank lines are saved as output results */
typedef boost::tokenizer<boost::char_separator<char>> tokenizer;
const boost::char_separator<char> DELIMETER
      {" 1234567890!@#$%^&*()_+=[{}]\\|;:'\"<>,./?"};

/* Edit distance related constants
 * to prevent magic numbers, the costs for inserting, deleting and replacing added
 */
const int INSERT_COST = 1;
const int DELETE_COST = 1;
const int REPLACE_COST = 1;

/* NOTE: Including program name we have 1 + 8 command line arguments
 * With the 8 command line args we have, 4 flags and 4 matching parameters:
 * FORMAT: [Flag] [Parameter]
 * - [-s] [datastructure], [-d] [dictionary], [-t] [textfile], [-o] [outputfile]
 * Datastructure types, input must be:
 * - custom_tree, custom_list, vector, set, list */

// Preventing magic numbers
const int NUM_DATA_STRUCTS = 5;
const int NUM_ARGS = 9;
const int NUM_FLAGS = 4;

// Index positions of arguments (When ordered), 0 is for the program name
const int S_FLAG_INDEX = 1;     const int DATASTRUCTRE_INDEX = 2;
const int D_FLAG_INDEX = 3;     const int DICTIONARY_INDEX = 4;
const int T_FLAG_INDEX = 5;     const int TEXT_FILE_INDEX = 6;
const int O_FLAG_INDEX = 7;     const int OUTPUT_FILE_INDEX = 8;

// Custom defined string arrays
static std::string flags[NUM_FLAGS] = { "-s", "-d", "-t", "-o" };
static std::string dataStructs[NUM_DATA_STRUCTS] =
  { "list", "vector", "set", "custom_list", "custom_tree" };

// Message to print if parameters are wrong in command line args
const std::string invalidArgsMessage =
              "[USAGE]: [-s] (custom_list | custom_tree | list | vector | set)\n"
              "         [-d] (.dat file)\n"
              "         [-t] (.txt file)\n"
              "         [-o] (any file name)\n";

// Strings to append to line in dictionary search when count = 0
const std::string outputDelim = ": ";
const std::string zeroCountMessage =
              " was not found in the dictionary. The closest matches were: ";

// Edit distance related functions
int min(int, int);
// int getEditDistance(std::string, std::string, int, int);
int getEditDistance(std::string, std::string);

#endif
