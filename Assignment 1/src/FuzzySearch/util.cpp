
/* COSC1254 - Programming using C++ - Assignment 1
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 */

#include "util.h"

/* Edit distance - I obtained the base code from:
 * http://www.csegeek.com/csegeek/view/tutorials/algorithms/dynamic_prog/dp_part3.php
 * and made small modifications to it to be more understandable for myself
 * https://nlp.stanford.edu/IR-book/html/htmledition/edit-distance-1.html, mentioned
 * in the specs helped to build understanding as to how the algorithm should look
 */

int min(int a, int b) {
  if (a < b)
    return a;
  else
    return b;
}

int getEditDistance(std::string source, std::string compared) {
  int editDistance = 0;
  int sourceLen = source.length();
  int compLen = compared.length();

  /* Getting edit distance is done in two steps:
   * [1]. Create + initialize 2d matrix storing costs per character when comparing
   * Rows = sourceLen, columns = compLen */
  /* NOTE: C++ doesnt allow variable length arrays, so ive used an int vector
   * instead store costs.
   */
  std::vector<std::vector<int> > cost(sourceLen, std::vector<int> (compLen, 0));
  for (int i=0; i < sourceLen; i++) {  // Source = row
    for(int j=0; j < compLen; j++) { // Compared = col
     /* [Case 1]: source string is "", we need to do insert all characters in
      * the compared string to get both to be equal */
      if (i == 0)
        cost[i][j] = j * INSERT_COST; // insert is weight 1, so j * 1 is basically just j
      /* [Case 2]: comparison string is "", so instead of case 1, we delete all
       * characters from source to get it to be equal */
      else if (j == 0)
        cost[i][j] = i * DELETE_COST; // delete is also weight 1, so i * 1 is i
      /* NOTE: IMPORTANT - Continue re-commenting here */
      else
        cost[i][j] = -1;
    }
  }

  /* [2]. Calculate actual costs
   * NOTE: -1 is used as arrays always start from 0 */
  for(int i=1; i < sourceLen; i++) {
     for(int j=1; j < compLen; j++) {
        int x = cost[i-1][j] + DELETE_COST;
        int y = cost[i][j-1] + INSERT_COST;
        int z = cost[i-1][j-1] + (source[i-1] != compared[j-1]) * REPLACE_COST;
        cost[i][j] = min(x, min(y,z));
     }
  }

  // Result to return is present in the bottom right corner cell
  editDistance = cost[sourceLen-1][compLen-1];
  return editDistance;
}
