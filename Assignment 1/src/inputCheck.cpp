
/* COSC1254 - Programming using C++ - Assignment 1
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 */

#include "inputCheck.h"

/* NOTE: I initially completed this without using boost, when implementing file I/O
 * was when i started using boost, and figured since this works anyways, i
 * might just leave it and not use tokenizing for the command line arguments
 */

using namespace std;

bool validSuffix(string str, string suffix) {
  // Function used to validate extension on dictionary/text file
  bool validSuffix = false;
  // 1. Check if size of string is actually large enough
  if (str.size() > suffix.size()) {
    // 2. Check if string then contains the suffix in it
    if (str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0)
      validSuffix = true;
  }
  return validSuffix;
}

vector<string> validateArgs(char * args[], vector<string> flags,
                                           vector<string> dsType) {
  bool valid = true;
  // Record index which flag has matched at and count number of instances foreach flag
  int flagArg = 0;
  vector<int> flagCount(NUM_FLAGS);
  // Sorted version of argv that will be returned based on flags[] input
  vector<string> output(NUM_ARGS);

  // Add program name
  output[0] = args[0];

  /* ########## VALID FLAG CHECKING ##########
   * i) Outer loop: Start with [-s] flag, look for it in argv (args)
   * ii) Inner loop: Compare one by one with 4 flags in argv, and count matches
   */
  for (int i = 0; i < NUM_FLAGS; i++) {
    for (int j = 1; j < NUM_ARGS; j+=2) {
      if (args[j] == flags[i]) {
        flagCount[i]++; // When match found increment count
        flagArg = j+1;  // Record value after flag to store in sorted array
      }
      if (flagCount[i] > 1) { // Error when second instance of flag has been found
        cout << "[ERROR]: Duplicate flag detected: " << flags[i] << endl;
        valid = false;
        break;
      }
    }
    // Flag has not been found, or there are too many occurences of the flag
    if (flagCount[i] == 0) {
      cout << "[ERROR]: Missing flag: [" << flags[i] << "]" << endl;
      valid = false;
      break;
    }
    // Only one instance of flag has been found, place it in orderedArray
    else {
      /* NOTE: Regarding the magic numbers below for (i * 2) + 1
       * (i * 2): due to args are stored in sets of two: [1, 2],[3, 4],[5, 6]
       * +1: adding the shift (1), we are skipping [0] (its added at the start) */
      output[(i * 2) + 1] = flags[i];      // Store flag in ordered array
      output[(i * 2) + 2] = args[flagArg]; // Store subsequent param
    }
  }

  /* ########## VALID PARAMETER CHECKING ##########
   * Given that the args are now ordered, it is easy to check whether their
   * parameters are also correct
   * Indexes:
   * [2] = data structure   * Must be: (list | vector | set | custom_list | custom_tree)
   * [4] = dictionary file  * End with .dat, length > than 5 characters (including extension)
   * [6] = textfile         * End with .txt, length > than 5 characters (including extension)
   * [8] = outputfile       * No restrictions
   */

  // Flags are valid, check params next
  if (valid) {
    // 1. Check whether valid data structure has been entered
    if (find(dsType.begin(), dsType.end(), output[DATASTRUCTRE_INDEX]) == dsType.end()) {
      cout << "[ERROR]: Invalid data structure: " << output[DATASTRUCTRE_INDEX] << endl;
      valid = false;
    }
    // 2. Check for valid dictionary file (check if last 4 characters is .dat)
    else if (!validSuffix(output[DICTIONARY_INDEX], ".dat")) {
      cout << "[ERROR]: Invalid dictionary file: " << output[DICTIONARY_INDEX] << endl;
      valid = false;
    }
    // 3. Check for valid text file (check if last 4 characters is .txt)
    else if (!validSuffix(output[TEXT_FILE_INDEX], ".txt")) {
      cout << "[ERROR]: Invalid text file: " << output[TEXT_FILE_INDEX] << endl;
      valid = false;
    }
  }

  // Outputfile will be saved as a .csv file, so .csv will be appended onto it
  output[OUTPUT_FILE_INDEX].append(".csv");

  if (!valid) {
    cout << invalidArgsMessage;
    exit(EXIT_FAILURE);
  }

  return output;
}
