
/* COSC1254 - Programming using C++ - Assignment 1
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 */

#ifndef CONTAINER_H
#define CONTAINER_H

#include "util.h"
#include "custom_list.h"
#include "custom_tree.h"

class container {
  std::string datastructure;  // String is just used as an identifier

  /* NOTE: In real world situations the dictionary set would only contain unique
   * keys, so i would list it as std::set, but i have no idea how the actual tests
   * will run, whether or not duplicate words will appear in the dictionary */

  /* All datastructures are initialized in container class, pairs are used
   * depending on the parameter passed in the command line
   * LEFT = textfile lines               |  RIGHT = dictionary terms */
  std::list<std::string> data_list;         std::list<std::string> dict_list;
  std::vector<std::string> data_vector;     std::vector<std::string> dict_vector;
  std::multiset<std::string> data_set;      std::multiset<std::string> dict_set;
  custom_list data_custom_list;             custom_list dict_custom_list;
  custom_tree data_custom_tree;             custom_tree dict_custom_tree;

  public:
    container(std::string newDataStruct) : datastructure(newDataStruct) {};
    void addTextData(std::string);
    void addDictData(std::string);
    void searchData(std::map<std::string, int> &, std::map<std::string, std::string> &);
};

#endif
