
/* COSC1254 - Programming using C++ - Assignment 1
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 * Code base + ideas gotten from file: tree.h provided in course example files
 */

#ifndef CUSTOM_TREE_H
#define CUSTOM_TREE_H

#include <memory>
#include <iostream>
#include "util.h"

class custom_tree {
  // Custom tree's inner class (node)
  class node {
    std::string data;   // String data being stored at the node
    int dataCount;      // Counting for duplicate lines
    int editDistance;   // Storing editDistance for fuzzy searching
    // Pointers to left/right and parent connections
    std::unique_ptr<node> left;
    std::unique_ptr<node> right;

    public:
      // Constructor: set newdata, null to both left/right nodes and count to 1
      node(std::string newdata) : data(newdata), dataCount(1), editDistance(0),
                                  left(nullptr), right(nullptr) {}
      std::string getData(void);
      int getCount(void);
      void add(std::string);
      void search(std::string, std::map<std::string, int> &,
                               std::map<std::string, std::string> &, int);
      void compare(std::map<std::string, int> &,
                   std::map<std::string, std::string> &, custom_tree &);
      friend class custom_tree; // Enables node to use variables from custom_tree
  };

  std::unique_ptr<node> root; // Custom tree's root

  public:
    // Default constructor for custom tree, set root to null
    custom_tree(void) : root(nullptr) {}
    node *getRoot(void);
    void add(std::string);
    void compareTree(std::map<std::string, int> &,
                     std::map<std::string, std::string> &, custom_tree &);
    void search(std::string, std::map<std::string, int> &,
                std::map<std::string, std::string> &, int);
};

#endif
