
/* COSC1254 - Programming using C++ - Assignment 1
 * Full Name        : Pacific Thai
 * Student Number   : s3429648
 */

/* EXAMPLE COMMAND:
 * ./test_datastructures -s custom_list -d data/dict.dat -t data/kaddath.txt -o output.txt */

#include "inputCheck.h"
#include "container.h"

using namespace std;

int main (int argc, char* argv[]) {
  // Data structure that has been selected
  string chosenDS;
  // Initialize arrays of vectors, array definitions are found in util.h
  vector<string> args(NUM_ARGS);
  vector<string> allFlags(flags, flags + sizeof(flags) / sizeof(string));
  vector<string> allDataStructs(dataStructs,
                                dataStructs + sizeof(dataStructs) / sizeof(string));

  // Check if enough arguments have been passed into the program
  if (argc != NUM_ARGS) {
    cout << invalidArgsMessage;
    exit(EXIT_FAILURE);
  }

  // Validate arguments further through strict requirements, store ordered version
  args = validateArgs(argv, allFlags, allDataStructs);

  // With valid command line arguments:
  // i) Obtain selected datastructure type and initialize container object
  chosenDS = args[DATASTRUCTRE_INDEX];
  container datastructures(chosenDS);
  // ii) Obtain files from command line args (argv) and create outputfile to write to
  ifstream dictionary(args[DICTIONARY_INDEX]);
  ifstream text(args[TEXT_FILE_INDEX]);
  ofstream output(args[OUTPUT_FILE_INDEX]);
  // iii) Initilize variables related to file operations
  map<string, string> extraMap; // Map storing extra info (edit distance words)
  map<string, int> dataMap;     // Map to store counts of data (required by specs)
  string key = "";              // Key related to map
  string extraInfo = "";        // Add delim (": ") OR (" word could not be found...")
  int value = 0;                // matching value pair for key in map
  string line = "";             // Reading files line by line

  // [1.] - Go through input text file and store all data in the datastructure
  if (text.is_open()) {
    while (getline(text, line))
      datastructures.addTextData(line); // Read file line by line and add to DS
    text.close(); // Close input text file once done with it
  } else { // Print error message if file cannot be opened
    cout << "[ERROR] - unable to open textfile: " << args[TEXT_FILE_INDEX] << endl;
    exit(EXIT_FAILURE);
  }
  cout << "[SYSTEM] - Loaded in text data complete..." << endl;
  // [2.] - Do the same with dictionary file, load it into a datastructure
  if (dictionary.is_open()) {
    while (getline(dictionary, line))
      datastructures.addDictData(line); // Read file line by line and add to DS
    dictionary.close(); // Close input text file once done with it
  } else { // Print error message if file cannot be opened
    cout << "[ERROR] - unable to open dictionary: " << args[DICTIONARY_INDEX] << endl;
    exit(EXIT_FAILURE);
  }
  cout << "[SYSTEM] - Loaded in dictionary data complete..." << endl;

  // [3.] Perform search with datastructures and store output in map
  datastructures.searchData(dataMap, extraMap);
  cout << "[SYSTEM] - Finished finding text counts in dictionary..." << endl;

  // [4.] - Go through output file and write out all data inside the dataMap
  if (output.is_open()) {
    auto dataEle = dataMap.begin();
    auto extraEle = extraMap.begin();
    // for (pair<string, int> element : dataMap) {
    /* Iterate through both dataMap and extraMap at the same time, since they both
     * use the same key */
    while (dataEle != dataMap.end()) {
      // Save key and value pairs for each element in the dataMap
      key = dataEle->first;
      value = dataEle->second;
      extraInfo = extraEle->second;
      // Write text info in: ([word:] OR [word could not be found...])
      output << key;
      // Only save count and the delimiter when it has been matched
      if (value > 0)
        output << outputDelim << value;
      else // Otherwise add the message that it could not be found and closest words
         output << zeroCountMessage << extraInfo;
      output << endl;    // End with a new line character

      // Increment to next map value
      ++dataEle;
      ++extraEle;
    }
    output.close(); // Close output file once done with it
  } else { // Print error message if file cannot be opened
    cout << "[ERROR] - unable to open output: " << args[OUTPUT_FILE_INDEX] << endl;
    exit(EXIT_FAILURE);
  }
  cout << "[SYSTEM] - Saving to output file complete..." << endl;
}
